﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;
using System.Threading;

public class Control : MonoBehaviour {

	int layerMask;
	int polygonSteps=360;
	 int closestObjIndex=0;
	public float sensorDist = 100;
	public float hitDistFront;
	public float hitDistBack;
	public float hitDistLeft;
	public float hitDistRight;
	 float objDistFront;
	 float objDistBack;
	 float objDistLeft;
	 float objDistRight;
	 float closestObjDist;
	public float maxVelocity;
	 float[] outputPolygon;
	 float[,] polygons;
	 float[] outputPolygon2;
	 float[] objDistSupportArray;
	 float[] baseLength;
	 float angleResult;
	 float c1;
	 float c2;
	 float cf1;
	 float cf2;
	 float cf3;
	 float angleClosest1;
	 float angleClosest2;
	 float finalAngle;
	 float final2;
	 float linearVariation;
	 float offsetLeft = 1.1f;
	 float offsetRight = 1.2f;
	 float offsetCenter = 1.1f;
	 float anglebias = 1.1f;
	 bool dodge;
	MembershipFunctions MF;
	Text infoText, infoText2, infoText3;
	ObjPhysics thisObj;
	 Vector3 translateVector;
	public Vector3 targetDirection;
	private ArrayList _obsMap;
	public ArrayList obsMap {
		get { return _obsMap; }
		set { _obsMap = value; }
	}

	 float min1 = 0, max1 = 0, min2 = 0, max2 = 0;
	public int maxcount = 0;
	public int maxcount2 = 0;

	public float targetForceMultiplier = 2;
	Vector3 fvec;
	Vector3 startPos;
	Vector3 movementVector;
	float blockSize;
	float cos45;
	float tan45;
	float maxChordDist;
	public float chordGranularity=5;
	public Vector2 targetPosition = new Vector2(8,5);
	public PathFind pf;
	Node[] path=null;
	int curPathPos=0;
	public Vector2 curTargetPos;
	public Vector2 curNodePos;
	public float forceBiasDivider=2;
	public float momentum = 0.3f;
	System.Timers.Timer t;
	public Vector2 curTargetCoord = new Vector2 (0, 0);
	public Vector2 validNodePos = new Vector2 (0, 0);

	bool pathfindStarted=false;
	bool newPathNeeded;
	bool targetAcquired;
	bool newTargetNeeded;
	bool timerStarted=false;
	bool targetReached=false;
	public bool validStartPosReached=true;
	bool movingToValidPos=false;
	bool onValidStartPos=true;
	public Main main;
	bool createNewObs;
	Vector2 ObsToCreate;

	public void toUpdate() {
		curNodePos = getCurPos ();
		ReadSensors();
		DodgeV2 ();
		BuildMap ();
		GetPathFind ();
		PathManagement ();
		if(createNewObs) {
			CreateObs(ObsToCreate);
			createNewObs=false;
		}
		if(movingToValidPos){
			MoveToValidPos();
		}
		curTargetCoord = new Vector2 (curTargetPos.x * blockSize, curTargetPos.y * blockSize);
		Translate ();
//		GetClosestObj();
//		SetVelocityLimit();
//		DodgeOrContinue();
//		if(dodge) {
//			//build polygon
//			//get centroid of reversed polygon
//			BuildOutputPolygon();
//			CalculateDodgeAngle ();
//			NormalizeDodgeAngle();
//			Dodge();
//		}
//		else {
//			Continue();
//		}
//
	}

	public void Start() {
		layerMask = 1<<9;
		MF=GameObject.Find("MainObj").GetComponent<MembershipFunctions>();
		MF.StartDefault();
		outputPolygon=new float[polygonSteps];
		infoText = GameObject.Find ("InfoText").GetComponent<Text>();
		infoText2=GameObject.Find("InfoText2").GetComponent<Text>();
		infoText3=GameObject.Find("InfoText3").GetComponent<Text>();
		thisObj=gameObject.GetComponent<ObjPhysics>();
		objDistSupportArray = new float[4];
		baseLength = new float[4];
		polygons=new float[MF.Mfsize, polygonSteps];
		startPos = transform.position;
		blockSize = sensorDist / 2;
		cos45=Mathf.Cos(Mathf.Deg2Rad*45);
		tan45=Mathf.Tan(Mathf.Deg2Rad*45);
		maxChordDist = 2 * (sensorDist * cos45);
		obsMap=new ArrayList();
		newPathNeeded = true;
		targetAcquired = false;
		newTargetNeeded = false;
	}

	public void Translate(){
		transform.Translate(movementVector);
	}

	#region Sensor Read V2
	public void SensorReadV2(){
		Vector3 dist;
		RaycastHit hit;
		Ray ray;
		hitDistFront=sensorDist+1;
		hitDistLeft=sensorDist+1;
		hitDistRight=sensorDist+1;
		hitDistBack=sensorDist+1;
		for(int i = 0;i<360;i++) {
			float f = i;
			dist=new Vector3(Mathf.Sin(Mathf.Deg2Rad*f), 0, Mathf.Cos(Mathf.Deg2Rad*f));
			ray=new Ray(transform.position, dist);
			/*if (i == 45 || i==135 || i==225 || i==315) {
				Debug.Log (i+" - "+dist.x);
				Debug.DrawRay (transform.position, dist * sensorDist, Color.blue);
			}*/
			if(Physics.Raycast(ray, out hit, sensorDist, layerMask)) {
				Debug.DrawRay(transform.position, dist*hit.distance, Color.red);
				if(i<45||i>=315){
					if(hitDistFront>hit.distance)
						hitDistFront=hit.distance;
				}
				if(i>=45&&i<135){
					if(hitDistRight>hit.distance)
						hitDistRight=hit.distance;
				}
				if(i>=135&&i<225){
					if(hitDistBack>hit.distance)
						hitDistBack=hit.distance;
				}
				if(i>=225&&i<315){
					if(hitDistLeft>hit.distance)
						hitDistLeft=hit.distance;
				}
			}
			else {
				Debug.DrawRay(transform.position, dist*sensorDist, Color.gray);
			}
		}
		if(hitDistFront<0)
			hitDistFront=hitDistFront*-1;
	}
	#endregion

	#region Sensor Read
	public void ReadSensors() {
		SensorReadV2 ();
		//ReadFrontSensor();
		//ReadBackSensor();
		//ReadLefttSensor();
		//ReadRightSensor();
	}
	public void ReadFrontSensor() {
		Vector3 vec;
		RaycastHit hit;
		Ray ray;
		hitDistFront=101;
		for(int i = 0;i<80;i++) {
			float f = i;
			vec=new Vector3((40-f)/40, 0, 1);
			ray=new Ray(transform.position, vec);
			if(Physics.Raycast(ray, out hit, sensorDist, layerMask)) {
				Debug.DrawRay(transform.position, vec*hit.distance, Color.red);
				if(hitDistFront==0) {
					hitDistFront=hit.distance;
				}
				else {
					if(hitDistFront>hit.distance) {
						hitDistFront=hit.distance;
					}
				}
			}
			else {
				Debug.DrawRay(transform.position, vec*sensorDist, Color.green);
			}
		}
		if(hitDistFront<0)
			hitDistFront=hitDistFront*-1;
		if(hitDistBack<0)
			hitDistBack=hitDistBack*-1;
		if(hitDistLeft<0)
			hitDistLeft=hitDistLeft*-1;
		if(hitDistRight<0)
			hitDistRight=hitDistRight*-1;
	}
	public void ReadBackSensor() {
		Vector3 vec;
		RaycastHit hit;
		Ray ray;
		hitDistBack=101;
		for(int i = 0;i<80;i++) {
			float f = i;
			vec=new Vector3((40-f)/40, 0, -1);
			ray=new Ray(transform.position, vec);
			if(Physics.Raycast(ray, out hit, sensorDist, layerMask)) {
				Debug.DrawRay(transform.position, vec*hit.distance, Color.red);
				if(hitDistBack==0) {
					hitDistBack=hit.distance;
				}
				else {
					if(hitDistBack>hit.distance) {
						hitDistBack=hit.distance;
					}
				}
			}
			else {
				Debug.DrawRay(transform.position, vec*sensorDist, Color.green);
			}
		}
		if(hitDistBack<0)
			hitDistBack=hitDistBack*-1;
	}
	public void ReadLefttSensor() {
		Vector3 vec;
		RaycastHit hit;
		Ray ray;
		hitDistLeft=101;
		for(int i = 0;i<80;i++) {
			float f = i;
			vec=new Vector3(-1, 0, (40-f)/40);
			ray=new Ray(transform.position, vec);
			if(Physics.Raycast(ray, out hit, sensorDist, layerMask)) {
				Debug.DrawRay(transform.position, vec*hit.distance, Color.red);
				if(hitDistLeft==0) {
					hitDistLeft=hit.distance;
				}
				else {
					if(hitDistLeft>hit.distance) {
						hitDistLeft=hit.distance;
					}
				}
			}
			else {
				Debug.DrawRay(transform.position, vec*sensorDist, Color.cyan);
			}
		}
		if(hitDistLeft<0)
			hitDistLeft=hitDistLeft*-1;
	}
	public void ReadRightSensor() {
		Vector3 vec;
		RaycastHit hit;
		Ray ray;
		hitDistRight=101;
		for(int i = 0;i<80;i++) {
			float f = i;
			vec=new Vector3(1, 0, (40-f)/40);
			ray=new Ray(transform.position, vec);
			if(Physics.Raycast(ray, out hit, sensorDist, layerMask)) {
				Debug.DrawRay(transform.position, vec*hit.distance, Color.red);
				if(hitDistRight==0) {
					hitDistRight=hit.distance;
				}
				else {
					if(hitDistRight>hit.distance) {
						hitDistRight=hit.distance;
					}
				}
			}
			else {
				Debug.DrawRay(transform.position, vec*sensorDist, Color.cyan);
			}
		}
		if(hitDistRight<0)
			hitDistRight=hitDistRight*-1;
	}
	#endregion

	#region DodgeV2
	public void DodgeV2(){
		Vector3[] forces = new Vector3[5];
		for(int i=0;i<5;i++){
			forces [i] = new Vector3 (0, 0, 0);
		}
		if(hitDistFront<sensorDist){
			forces [0] = new Vector3 ((1-(hitDistFront/sensorDist))/forceBiasDivider, 0, -(1-(hitDistFront/sensorDist)));
		}
		if(hitDistBack<sensorDist){
			forces [1] = new Vector3 (-(1-(hitDistBack/sensorDist))/forceBiasDivider, 0, 1-(hitDistBack/sensorDist));
		}
		if(hitDistRight<sensorDist){
			forces [2] = new Vector3 (-(1-(hitDistRight/sensorDist)), 0, (1-(hitDistRight/sensorDist))/forceBiasDivider);
		}
		if(hitDistLeft<sensorDist){
			forces [3] = new Vector3 (1-(hitDistLeft/sensorDist), 0, -(1-(hitDistLeft/sensorDist))/forceBiasDivider);
		}
		forces [4] = targetDirection*targetForceMultiplier;

		string s = "";
		fvec = new Vector3 (0, 0, 0);
		for (int i = 0; i < 5; i++) {
			fvec += forces [i];
			s += forces [i].ToString () + " - ";
		}
		float f1 = fvec.x;
		if (f1 < 0)
			f1 = -f1;
		float f2 = fvec.z;
		if (f2 < 0)
			f2 = -f2;
		float f = f1 + f2;
		if (f != 0)
			fvec = new Vector3 (fvec.x / f, 0, fvec.z / f);
//		Vector3 lastMovVec = thisObj.movementVector / maxVelocity;
//		if (Mathf.Abs(Mathf.Abs (lastMovVec.x) - Mathf.Abs (fvec.x)) > momentum){
//			if(fvec.x>0){
//				fvec.x = lastMovVec.x + momentum;
//			}
//			else{
//				fvec.x = lastMovVec.x - momentum;
//			}
//		}
//		if (Mathf.Abs(Mathf.Abs (lastMovVec.y) - Mathf.Abs (fvec.y)) > momentum){
//			if(fvec.y>0){
//				fvec.y = lastMovVec.y + momentum;
//			}
//			else{
//				fvec.y = lastMovVec.y - momentum;
//			}
//		}
		//Debug.Log (fvec.ToString ()+" lastMom:"+lastMovVec.ToString()+" for:"+s);
		thisObj.movementVector=fvec*maxVelocity;
		GetClosestObj();
		Debug.DrawRay(transform.position, fvec*10, Color.cyan);
//		for(int i=0;i<5;i++){
//			forces [i] = new Vector3 (0, 0, 0);
//		}
//		if(hitDistFront<(sensorDist/targetForceMultiplier)){
//			forces [0] = new Vector3 (0, 0, -((sensorDist/targetForceMultiplier) - hitDistFront));
//		}
//		if(hitDistBack<(sensorDist/targetForceMultiplier)){
//			forces [1] = new Vector3 (0, 0, (sensorDist/targetForceMultiplier) - hitDistBack);
//		}
//		if(hitDistRight<(sensorDist/targetForceMultiplier)){
//			forces [2] = new Vector3 (-((sensorDist/targetForceMultiplier) - hitDistRight), 0, 0);
//		}
//		if(hitDistLeft<(sensorDist/targetForceMultiplier)){
//			forces [3] = new Vector3 ((sensorDist/targetForceMultiplier) - hitDistLeft, 0, 0);
//		}
//		Vector3 ntarg = targetDirection;
//		Vector3[] tVec = new Vector3[4];
//		tVec[0] = new Vector3 (0, 0, 1);
//		tVec[1] = new Vector3 (0, 0, -1);
//		tVec[2] = new Vector3 (1, 0, 0);
//		tVec[3] = new Vector3 (-1, 0, 0);
//		if(ntarg==tVec[0]&&hitDistFront<sensorDist){
//			ntarg = new Vector3 (0.1f, 0, 0.9f);
//		}
//		if(ntarg==tVec[1]&&hitDistBack<sensorDist){
//			ntarg= new Vector3 (-0.1f, 0, -0.9f);
//		}
//		if(ntarg==tVec[2]&&hitDistRight<sensorDist){
//			ntarg = new Vector3 (0.9f, 0, -0.1f);
//		}
//		if(ntarg==tVec[3]&&hitDistLeft<sensorDist){
//			ntarg = new Vector3 (-0.9f, 0, 0.1f);
//		}
//		forces [4] = (ntarg * (sensorDist/targetForceMultiplier))/2;
//
//		fvec = new Vector3 (0, 0, 0);
//		for(int i=0;i<5;i++){
//			forces [i] = forces [i] / (sensorDist/targetForceMultiplier);
//			fvec += forces [i];
//		}
//		string s = "";
//		for(int i =0;i<5;i++){
//			s += forces[i].ToString() + " | ";
//		}
//		s += fvec + " || ";
//
//		float f1 = fvec.x;
//		if (f1 < 0)
//			f1 = -f1;
//		float f2 = fvec.z;
//		if (f2 < 0)
//			f2 = -f2;
//		float f = f1 + f2;
//		if(f<0)
//			f = -f;
//		
//		s += f + ", ";
//		if (f != 0) 
//			fvec = new Vector3 (fvec.x / f, 0, fvec.z / f);
//
//		s+=fvec.x+", "+fvec.z+" ";
//		thisObj.movementVector=fvec*maxVelocity;
//		Debug.DrawRay(transform.position, fvec*10, Color.cyan);
	}
	#endregion

	#region Build Map
	public void CreateObs(Vector2 obsToCreate) {
		obsMap.Add(obsToCreate);
		main.obs.Add(obsToCreate);
		main.newObs=true;
		GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
		Vector2 r = PosToCoord(obsToCreate);
		cube.transform.position=new Vector3(r.y, -2, r.x);
		cube.transform.localScale=new Vector3(blockSize, 1, blockSize);
	}

	public Vector2 CoordToPos(Vector2 coord){
		float bsize = blockSize / 2;
		float p1 = coord.x;
		if(p1<0){
			bsize = -bsize;
		}
		p1 = (p1+bsize) / blockSize;
		if (p1 < 0)
			p1 = Mathf.Ceil (p1);
		else
			p1 = Mathf.Floor (p1);
		float p2 = coord.y;
		if(p2<0){
			bsize = -bsize;
		}
		p2 = (p2+bsize) / blockSize;
		if (p2 < 0)
			p2 = Mathf.Ceil (p2);
		else
			p2 = Mathf.Floor (p2);
		Vector2 r = new Vector2 (p1,p2);
		return r;
	}

	public Vector2 PosToCoord(Vector2 pos){
		Vector2 r = new Vector2(pos.x*blockSize,pos.y*blockSize);
		return r;
	}

	public Vector3 V2CoordToV3(Vector2 coord){
		Vector3 r = new Vector3 (coord.y, 0, coord.x);
		return r;
	}

	public Vector2 getCurCoord(){
		Vector2 r = new Vector2 (transform.position.z, transform.position.x);
		return r;
	}

	public Vector2 getCurPos(){
		Vector2 r = new Vector2 (transform.position.z, transform.position.x);
		float bsize = blockSize / 2;
		float p1 = r.x;
		if(p1<0){
			bsize = -bsize;
		}
		p1 = (p1+bsize) / blockSize;
		if (p1 < 0)
			p1 = Mathf.Ceil (p1);
		else
			p1 = Mathf.Floor (p1);
		float p2 = r.y;
		bsize = blockSize / 2;
		if(p2<0){
			bsize = -bsize;
		}
		p2 = (p2+bsize) / blockSize;
		if (p2 < 0)
			p2 = Mathf.Ceil (p2);
		else
			p2 = Mathf.Floor (p2);
		r = new Vector2 (p1,p2);
		return r;
	}

	public void BuildMap(){
		Vector2 blockPos= new Vector2(0,0);
		if (hitDistFront < sensorDist) {
			float ox = transform.position.z;
			float oy = transform.position.x;
			float x = hitDistFront;
			float y = 0;
			for(int i=0;i<chordGranularity+1;i++){
				float angle = 45 - ((90/chordGranularity) *i);
				float bsize;
				if (angle < 0)
					bsize = -blockSize / 2;
				else
					bsize = blockSize / 2;
				float xi = x * Mathf.Cos (Mathf.Deg2Rad * angle);
				float yi= x * Mathf.Sin (Mathf.Deg2Rad * angle);
				float xx = xi;
				float yy = yi;
				xi += ox;
				yi += oy;
				Debug.DrawRay (transform.position, new Vector3 (yy,0,xx), Color.blue);
				float p1 = (xi+(blockSize/2)) / blockSize;
				if (p1 < 0)
					p1 = Mathf.Ceil (p1);
				else
					p1 = Mathf.Floor (p1);
				float p2 = (yi+bsize) / blockSize;
				if (p2 < 0)
					p2 = Mathf.Ceil (p2);
				else
					p2 = Mathf.Floor (p2);
				blockPos = new Vector2 (p1,p2);
				bool b = false;
				for (int j = 0; j < obsMap.Count; j++) {
					if (obsMap.Contains (blockPos)) {
						b = true;
					}
				}
				if (!b) {
					obsMap.Add (blockPos);
					main.obs.Add (blockPos);
					main.newObs = true;
					for(int j=curPathPos;j<path.Length;j++){
						if(blockPos==path[j].pos){
							Debug.Log ("ERROR NODE OCCUPIED");
							CalcNewPath ();
						}
					}
					GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
					Vector2 r = PosToCoord (blockPos);
					cube.transform.position = new Vector3 (r.y, -2, r.x);
					cube.transform.localScale = new Vector3 (blockSize, 1, blockSize);
				}
			}
		}
		if (hitDistBack < sensorDist) {
			float ox = transform.position.z;
			float oy = transform.position.x;
			float x = hitDistBack;
			float y = 0;
			for(int i=0;i<chordGranularity+1;i++){
				float angle = 45 - ((90/chordGranularity) *i);
				float bsize;
				if (angle < 0)
					bsize = blockSize / 2;
				else
					bsize = -blockSize / 2;
				float xi = -x * Mathf.Cos (Mathf.Deg2Rad * angle);
				float yi= -x * Mathf.Sin (Mathf.Deg2Rad * angle);
				float xx = xi;
				float yy = yi;
				xi += ox;
				yi += oy;
				Debug.DrawRay (transform.position, new Vector3 (yy,0,xx), Color.blue);
				float p1 = (xi-(blockSize/2)) / blockSize;
				if (p1 < 0)
					p1 = Mathf.Ceil (p1);
				else
					p1 = Mathf.Floor (p1);
				float p2 = (yi+bsize) / blockSize;
				if (p2 < 0)
					p2 = Mathf.Ceil (p2);
				else
					p2 = Mathf.Floor (p2);
				blockPos = new Vector2 (p1,p2);
				bool b = false;
				for (int j = 0; j < obsMap.Count; j++) {
					if (obsMap.Contains (blockPos)) {
						b = true;
					}
				}
				if (!b) {
					obsMap.Add (blockPos);
					main.obs.Add (blockPos);
					main.newObs = true;
					for(int j=curPathPos;j<path.Length;j++){
						if(blockPos==path[j].pos){
							Debug.Log ("ERROR NODE OCCUPIED");
							CalcNewPath ();
						}
					}
					GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
					Vector2 r = PosToCoord (blockPos);
					cube.transform.position = new Vector3 (r.y, -2, r.x);
					cube.transform.localScale = new Vector3 (blockSize, 1, blockSize);
				}
			}
		}
		if (hitDistRight < sensorDist) {
			float ox = transform.position.z;
			float oy = transform.position.x;
			float x = 0;
			float y = hitDistRight;
			for(int i=0;i<chordGranularity+1;i++){
				float angle = 45 - ((90/chordGranularity) *i);
				float bsize;
				if (angle < 0)
					bsize = blockSize / 2;
				else
					bsize = -blockSize / 2;
				float xi = -y * Mathf.Sin (Mathf.Deg2Rad * angle);
				float yi= y * Mathf.Cos (Mathf.Deg2Rad * angle);
				float xx = xi;
				float yy = yi;
				xi += ox;
				yi += oy;
				Debug.DrawRay (transform.position, new Vector3 (yy,0,xx), Color.blue);
				float p1 = (xi+bsize) / blockSize;
				if (p1 < 0)
					p1 = Mathf.Ceil (p1);
				else
					p1 = Mathf.Floor (p1);
				float p2 = (yi+(blockSize/2)) / blockSize;
				if (p2 < 0)
					p2 = Mathf.Ceil (p2);
				else
					p2 = Mathf.Floor (p2);
				blockPos = new Vector2 (p1,p2);
				bool b = false;
				for (int j = 0; j < obsMap.Count; j++) {
					if (obsMap.Contains (blockPos)) {
						b = true;
					}
				}
				if (!b) {
					obsMap.Add (blockPos);
					main.obs.Add (blockPos);
					main.newObs = true;
					for(int j=curPathPos;j<path.Length;j++){
						if(blockPos==path[j].pos){
							Debug.Log ("ERROR NODE OCCUPIED");
							CalcNewPath ();
						}
					}
					GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
					Vector2 r = PosToCoord (blockPos);
					cube.transform.position = new Vector3 (r.y, -2, r.x);
					cube.transform.localScale = new Vector3 (blockSize, 1, blockSize);
				}
			}
		}
		if (hitDistLeft < sensorDist) {
			float ox = transform.position.z;
			float oy = transform.position.x;
			float x = 0;
			float y = hitDistLeft;
			for(int i=0;i<chordGranularity+1;i++){
				float angle = 45 - ((90/chordGranularity) *i);
				float bsize;
				if (angle < 0)
					bsize = -blockSize / 2;
				else
					bsize = blockSize / 2;
				float xi = y * Mathf.Sin (Mathf.Deg2Rad * angle);
				float yi= -y * Mathf.Cos (Mathf.Deg2Rad * angle);
				float xx = xi;
				float yy = yi;
				xi += ox;
				yi += oy;
				Debug.DrawRay (transform.position, new Vector3 (yy,0,xx), Color.blue);
				float p1 = (xi+bsize) / blockSize;
				if (p1 < 0)
					p1 = Mathf.Ceil (p1);
				else
					p1 = Mathf.Floor (p1);
				float p2 = (yi-(blockSize/2)) / blockSize;
				if (p2 < 0)
					p2 = Mathf.Ceil (p2);
				else
					p2 = Mathf.Floor (p2);
				blockPos = new Vector2 (p1,p2);
				bool b = false;
				for (int j = 0; j < obsMap.Count; j++) {
					if (obsMap.Contains (blockPos)) {
						b = true;
					}
				}
				if (!b) {
					obsMap.Add (blockPos);
					main.obs.Add (blockPos);
					main.newObs = true;
					for(int j=curPathPos;j<path.Length;j++){
						if(blockPos==path[j].pos){
							Debug.Log ("ERROR NODE OCCUPIED");
							CalcNewPath ();
						}
					}
					GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
					Vector2 r = PosToCoord (blockPos);
					cube.transform.position = new Vector3 (r.y, -2, r.x);
					cube.transform.localScale = new Vector3 (blockSize, 1, blockSize);
				}
			}
		}
	}
	#endregion

	#region Path Find
	public void MoveToValidPos(){
		curTargetPos = validNodePos;
		Vector2 tpos = new Vector2 (curTargetPos.x * blockSize, curTargetPos.y * blockSize);
		float dx = tpos.x - transform.position.z;
		float dy = tpos.y - transform.position.x;
		float angle = Mathf.Rad2Deg * Mathf.Atan2 (dy, dx);
		Vector2 direction = new Vector2 (Mathf.Cos (Mathf.Deg2Rad * angle), Mathf.Sin (Mathf.Deg2Rad * angle));
		targetDirection = new Vector3 (direction.y, 0, direction.x);
		float dist = Mathf.Sqrt (Mathf.Pow (dx, 2) + Mathf.Pow (dy, 2));
				//Debug.Log ("tpos"+tpos.ToString ()+" cpos:"+transform.position.ToString());
		if (dist < blockSize / 2) {
			validStartPosReached = true;
			onValidStartPos = true;
			movingToValidPos = false;
			pathfindStarted = false;
			Debug.Log ("NEW VALID POS REACHED");
			CalcNewPath ();
		}
	}

	public void WaitForNewValidStartPos(){
		timerStarted = false;
		t.Stop ();
		newPathNeeded = false;
		targetAcquired = false;
		onValidStartPos = false;
		if(validStartPosReached){
			movingToValidPos = true;
		}
	}

	public void CalcNewPath(){
		if (onValidStartPos) {
			timerStarted = false;
			t.Stop ();
			newPathNeeded = true;
			targetAcquired = false;
			Debug.Log ("NEW PATH NEEDED");
		}
	}

	public void GetPathFind(){
		if (!pathfindStarted&&newPathNeeded){
			Debug.Log ("INITIATING PATHFIND");
			pf.Initiate ();
			pathfindStarted = true;
		}
		if (pf.solutionReady&&newPathNeeded) {
			path = pf.GetPath ();
			curPathPos = 0;
			string s = "";
			for(int i=0;i<path.Length;i++){
				s += path [i].pos.ToString() + " - ";
			}
			Debug.Log ("PATH ACQUIRED "+s);
			newPathNeeded = false;
			newTargetNeeded = true;
			pathfindStarted = false;
		}
	}

	public Vector2 GetCurrentPos(){
		Vector2 pos = new Vector2 (transform.position.z, transform.position.x);
		float p1 = pos.x;
		float p2 = pos.y;
		if (p1 < 0)
			p1 = Mathf.Ceil ((p1-(blockSize/2)) / blockSize);
		else
			p1 = Mathf.Floor ((p1+(blockSize/2)) / blockSize);
		if (p2 < 0)
			p2 = Mathf.Ceil ((p2-(blockSize/2)) / blockSize);
		else
			p2 = Mathf.Floor ((p2+(blockSize/2)) / blockSize);
		return pos;
	}
	#endregion

	#region Path Manager
	public void PathManagement(){
		if (!targetReached) {
			if (newTargetNeeded) {
				curTargetPos = path [curPathPos].pos;
				newTargetNeeded = false;
				targetAcquired = true;
				Debug.Log ("TARGET ACQUIRED");
			}
			if (targetAcquired) {
				if (!timerStarted) {
					t = new System.Timers.Timer (3000);
					t.Elapsed += OnTimedEvent;
					t.Start ();
					timerStarted = true;
				}
				Vector2 tpos = new Vector2 (curTargetPos.x * blockSize, curTargetPos.y * blockSize);
				float dx = tpos.x - transform.position.z;
				float dy = tpos.y - transform.position.x;
				float angle = Mathf.Rad2Deg * Mathf.Atan2 (dy, dx);
				Vector2 direction = new Vector2 (Mathf.Cos (Mathf.Deg2Rad * angle), Mathf.Sin (Mathf.Deg2Rad * angle));
				targetDirection = new Vector3 (direction.y, 0, direction.x);
				float dist = Mathf.Sqrt (Mathf.Pow (dx, 2) + Mathf.Pow (dy, 2));
				//Debug.Log ("tpos"+tpos.ToString ()+" cpos:"+transform.position.ToString());
				if (dist < blockSize / 2) {
					if (curTargetPos == targetPosition) {
						timerStarted = false;
						t.Stop ();
						newPathNeeded = false;
						targetAcquired = false;
						targetReached = true;
						Debug.Log ("----------TARGET REACHED-------------");
					}
					if (!targetReached) {
						curPathPos++;
						curTargetPos = path [curPathPos].pos;
						t.Stop ();
						t = new System.Timers.Timer (3000);
						t.Elapsed += OnTimedEvent;
						bool b = true;
						for(int i=0;i<obsMap.Count;i++){
							if(curTargetPos==(Vector2)obsMap[i]){
								b = false;
							}
						}
						if(!b){
							Debug.Log ("ERROR NODE OCCUPIED");
							CalcNewPath ();
						}
						t.Start ();
					}
				}
			} else {
				targetDirection = new Vector3 (0, 0, 0);
			}
		}
		else {
			targetDirection = new Vector3 (0, 0, 0);
		}
	}

	private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)
	{
		if (timerStarted) {
			ObsToCreate=curTargetPos;
			createNewObs=true;
			t.Stop ();
			CalcNewPath ();
			Debug.Log ("TIMEOUT");
		}

	}
	#endregion

	#region Get Closest Obj
	public void GetClosestObj() {
		closestObjDist=hitDistFront;
		if(hitDistFront==0)
			closestObjIndex=0;
		else
			closestObjIndex=1;
		if(closestObjDist>hitDistBack) {
			closestObjIndex=2;
			closestObjDist=hitDistBack;
		}
		if(closestObjDist>hitDistLeft) {
			closestObjIndex=3;
			closestObjDist=hitDistLeft;
		}
		if(closestObjDist>hitDistRight) {
			closestObjIndex=4;
			closestObjDist=hitDistRight;
		}
	}
	#endregion

	#region Set Vel Limit
	public void SetVelocityLimit() {
		bool brk = false;
		if(!brk) {
			if(closestObjDist>=MF.MaxVelPerDist[2, 1]) {
				maxVelocity=MF.MaxVelPerDist[2, 0];
				brk=true;
			}
		}
		if(!brk) {
			if((closestObjDist<=MF.MaxVelPerDist[0, 1])) {
				maxVelocity=MF.MaxVelPerDist[0, 0];
				brk=true;
			}
		}
		if(!brk) {
			if(closestObjDist==MF.MaxVelPerDist[1, 1]) {
				maxVelocity=MF.MaxVelPerDist[1, 0];
				brk=true;
			}
		}
		if(!brk) {
			if(closestObjDist>MF.MaxVelPerDist[1, 1]&&closestObjDist<MF.MaxVelPerDist[2, 1]) {
				float d1 = MF.MaxVelPerDist[2, 1]-MF.MaxVelPerDist[1, 1];
				float d2 = MF.MaxVelPerDist[2, 0]-MF.MaxVelPerDist[1, 0];
				float dDist = closestObjDist-MF.MaxVelPerDist[1, 1];
				maxVelocity=((dDist/d1)*d2)+MF.MaxVelPerDist[1, 0];
				brk=true;
			}
		}
		if(!brk) {
			if(closestObjDist>MF.MaxVelPerDist[0, 1]&&closestObjDist<MF.MaxVelPerDist[1, 1]) {
				float d1 = MF.MaxVelPerDist[1, 1]-MF.MaxVelPerDist[0, 1];
				float d2 = MF.MaxVelPerDist[0, 0]-MF.MaxVelPerDist[1, 0];
				float dDist = 1-(closestObjDist-MF.MaxVelPerDist[0, 1]);
				maxVelocity=((dDist/d1)*d2)+MF.MaxVelPerDist[0, 0];
				brk=true;
			}
		}
		if(maxVelocity>100)
			maxVelocity=100;
	}
	#endregion

	#region Dodge or Continue
	public void Dodge() {
		Vector3 v = new Vector3(Mathf.Sin(Mathf.Deg2Rad*finalAngle), 0, Mathf.Cos(Mathf.Deg2Rad*finalAngle));
		v=v*(maxVelocity/200);
		thisObj.movementVector=v;
		translateVector=v;
		Debug.DrawRay(transform.position, v*100, Color.blue);
	}

	public void Continue() {
		objDistFront=100;
		objDistBack=100;
		objDistLeft=100;
		objDistRight=100;
		Vector3 v = targetDirection;
		v=v*(maxVelocity/100);
		thisObj.movementVector=v;
		translateVector=v;
		Debug.DrawRay(transform.position, v*100, Color.black);
	}
	public void DodgeOrContinue() {
		if(closestObjDist>MF.dodgeContinueThreshold) {
			dodge=false;
		}
		else {
			dodge=true;
		}
	}
	#endregion

	#region Build Output Polygon
	public void BuildOutputPolygon() {

		/*
		float[] supArray = new float[4];
		supArray[0]=objDistFront;
		supArray[1]=objDistRight;
		supArray[2]=objDistBack;
		supArray[3]=objDistLeft;
		for(int i = 0;i<4;i++) {
			if(supArray[i]>60) {
				supArray[i]=60;
			}
		}


		for(int i = 0;i<polygonSteps;i++){
			for(int j = 0;j<MF.Mfsize;j++) {
				float res;
				if(MF.MF[j, 0]==i)
					res=0;
				if(MF.MF[j, 1]==i)
					res=1;
				if(MF.MF[j, 2]==i)
					res=0;
				float bl=
				if(MF.MF[j, 0]<i) {
					res=MF.MF[j, 0]-MF.MF[j, 1];
					res=1-((i-MF.MF[j,1])*(supArray[(int)MF.MF[j, 3]]/60));
				}
					

				//polygons[j,i]=
			}
		}*/



		infoText.text="";
		int frontSupportIndex=4;
		if(hitDistFront>100) {
			objDistFront=100;
		}
		else {
			objDistFront=hitDistFront;
		}
		if(hitDistBack>100) {
			objDistBack=100;
		}
		else {
			objDistBack=hitDistBack;
		}
		if(hitDistLeft>100) {
			objDistLeft=100;
		}
		else {
			objDistLeft=hitDistLeft;
		}
		if(hitDistRight>100) {
			objDistRight=100;
		}
		else {
			objDistRight=hitDistRight;
		}
		float frontAngleMin=0, frontAngleMax = 0;
		switch(closestObjIndex) {
			case 0:
				for(int i = 0;i<polygonSteps;i++) {
					outputPolygon[i]=0;
				}
				break;
			case 1:
				frontAngleMin=270;frontAngleMax=90;
				frontSupportIndex=0;
				objDistSupportArray[0]=objDistFront;
				objDistSupportArray[1]=objDistRight;
				objDistSupportArray[2]=objDistBack;
				objDistSupportArray[3]=objDistLeft;
				break;
			case 2:
				frontAngleMin=90; frontAngleMax=270;
				frontSupportIndex=2;
				objDistSupportArray[0]=objDistBack;
				objDistSupportArray[1]=objDistLeft;
				objDistSupportArray[2]=objDistFront;
				objDistSupportArray[3]=objDistRight;
				break;
			case 3:
				frontAngleMin=0; frontAngleMax=180;
				frontSupportIndex=1;
				objDistSupportArray[0]=objDistLeft;
				objDistSupportArray[1]=objDistFront;
				objDistSupportArray[2]=objDistRight;
				objDistSupportArray[3]=objDistBack;
				break;
			case 4:
				frontAngleMin=180; frontAngleMax=360;
				frontSupportIndex=3;
				objDistSupportArray[0]=objDistRight;
				objDistSupportArray[1]=objDistBack;
				objDistSupportArray[2]=objDistLeft;
				objDistSupportArray[3]=objDistFront;
				break;
		}
		for(int i = 0;i<4;i++) {
			baseLength[i]=((1-(objDistSupportArray[i]/MF.dodgeContinueThreshold))*180)/2;
			if(i==frontSupportIndex) {
				baseLength[i]=baseLength[i]*offsetLeft;
				objDistSupportArray[i]=objDistSupportArray[i]*offsetCenter;
			}
			if(objDistSupportArray[i]>MF.dodgeContinueThreshold)
				objDistSupportArray[i]=MF.dodgeContinueThreshold;
		}
		if(closestObjDist!=0) {
			float out1 = 0, out2 = 0;
			int decreasingMF = 0, increasingMF = 0;
			float leftOffset = 0;
			infoText.text="";
			for(int i = 0;i<polygonSteps;i++) {
				float f = i;
				if(i<90) {
					decreasingMF=0;
					increasingMF=1;
					leftOffset=0;
				}
				if(i>=90&&i<180) {
					decreasingMF=1;
					increasingMF=2;
					leftOffset=90;
				}
				if(i>=180&&i<270) {
					decreasingMF=2;
					increasingMF=3;
					leftOffset=180;
				}
				if(i>=270) {
					decreasingMF=3;
					increasingMF=0;
					leftOffset=270;
				}
				float baseLengthD = 0, baseLengthI = 0;
				baseLengthD=baseLength[decreasingMF];
				baseLengthI=baseLength[increasingMF];
				if(i<frontAngleMax&&i<(frontAngleMax-90)) {
					baseLengthD=baseLengthD*offsetRight;
				}
				if(i>frontAngleMin&&i<(frontAngleMin+90)) {
					// baseLengthI=baseLengthI*offsetLeft;
				}
				if((i-leftOffset)<(baseLengthD)) {
					out1=1-((1-((f-leftOffset)/baseLengthD))*(1-(objDistSupportArray[decreasingMF]/MF.dodgeContinueThreshold)));
				}
				else {
					out1=1;
				}
				if((i-leftOffset)>(90-baseLengthI)) {
					out2=1-((1-((90-(f-leftOffset))/baseLengthI))*(1-(objDistSupportArray[increasingMF]/MF.dodgeContinueThreshold)));
				}
				else {
					out2=1;
				}
				if(out1>out2) {
					outputPolygon[i]=out2;
				}
				else {
					outputPolygon[i]=out1;
				}
				if(outputPolygon[i]>1){
					outputPolygon[i]=1;
				}
				infoText.text+=outputPolygon[i].ToString("F2")+";";
			}
		}
	}
	#endregion

	#region Calculate Dodge Angle
	public void CalculateDodgeAngle() {
		float[,] line = new float[2, 2];
		List<string> lineList = new List<string>();
		line[0, 0]=2;
		line[0, 1]=2;
		line[1, 0]=2;
		line[1, 1]=2;
		int start = 0;
		float heighest = 0;
		for(int i = 0;i<polygonSteps;i++) {
			if(outputPolygon[i]>heighest)
				heighest=outputPolygon[i];
			if(outputPolygon[i]!=line[0, 1]) {
				if((i-start)>1) {
					line[1, 0]=i-1;
					line[1, 1]=outputPolygon[i-1];
					lineList.Add(line[0, 0]+";"+line[0, 1]+";"+line[1, 0]+";"+line[1, 1]);
					line[0, 0]=0;
					line[0, 1]=0;
					line[1, 0]=0;
					line[1, 1]=0;
				}
				start=i;
				line[0, 1]=outputPolygon[i];
				line[0, 0]=i;
			}
		}
		float[,] lines = new float[lineList.Count, 2];
		string[] s = new string[4];
		if(lineList.Count>0) {
			for(int i = 0;i<lineList.Count;i++) {
				s=lineList[i].Split(';');
				float length = float.Parse(s[2])-float.Parse(s[0]);
				float heigth = float.Parse(s[1]);
				lines[i, 0]=length;
				lines[i, 1]=heigth;
			}

			float maxHeigth = 0;
			float maxLength = 0;
			int resultIndex = 0;
			for(int i = 0;i<lineList.Count;i++) {
				if(lines[i, 1]>maxHeigth) {
					maxHeigth=lines[i, 1];
					maxLength=lines[i, 0];
					resultIndex=i;
				}
				if(lines[i, 1]==maxHeigth) {
					if(lines[i, 0]>maxLength) {
						maxHeigth=lines[i, 1];
						maxLength=lines[i, 0];
						resultIndex=i;
					}
				}
			}
			s=lineList[resultIndex].Split(';');
			angleResult=((float.Parse(s[2])-float.Parse(s[0]))/2)+float.Parse(s[0]);
			angleClosest1=float.Parse(s[2]);
			angleClosest2=float.Parse(s[0]);
		}
		else {
			angleResult=heighest;
			angleClosest1=heighest;
			angleClosest2=heighest;
		}
	}
	#endregion

	#region Normalize Dodge Angle
	public void NormalizeDodgeAngle() {
		c1=angleClosest1;
		c2=angleClosest2;
		float rs = 1-(closestObjDist/MF.dodgeContinueThreshold);
		float rsf = angleClosest1-angleClosest2;
		linearVariation=rs;
		if(rsf<0)
			rsf=rsf*-1;
		rsf=rsf*rs;
		if(angleClosest1>angleClosest2) {
			rsf=angleClosest2*rsf;
		}
		else {
			rsf=angleClosest1*rsf;
		}
		cf2=angleResult;
		switch(closestObjIndex) {
			case 0:
				finalAngle=0;
				break;
			case 1:
				finalAngle=angleResult;
				break;
			case 2:

				if(angleResult<180) {
					finalAngle=angleResult+180;
				}
				else {
					finalAngle=angleResult-180;
				}

				/*finalAngle=angleResult+180;
				
				if(finalAngle>360) {
					finalAngle=finalAngle-270;
				}*/
				angleClosest1=angleClosest1+180;
				if(angleClosest1>360) {
					angleClosest1=angleClosest1-270;
				}
				angleClosest2=angleClosest2+180;
				if(angleClosest1>360) {
					angleClosest2=angleClosest2-270;
				}

				rsf=rsf+180;
				if(rsf>360) {
					rsf=rsf-270;
				}
				break;
			case 3:
				finalAngle=angleResult-90;
				if(finalAngle<0)
					finalAngle=finalAngle+360;

				angleClosest1=angleClosest1-90;
				if(angleClosest1<0) {
					angleClosest1=angleClosest1+360;
				}
				angleClosest2=angleClosest2-90;
				if(angleClosest2<0) {
					angleClosest2=angleClosest2+360;
				}

				rsf=rsf-90;
				if(rsf<0) {
					rsf=rsf+360;
				}
				break;
			case 4:
				finalAngle=angleResult+90;
				if(finalAngle>360) {
					finalAngle=finalAngle-360;
				}

				angleClosest1=angleClosest1+90;
				if(angleClosest1>360) {
					angleClosest1=angleClosest1-360;
				}
				angleClosest2=angleClosest2+90;
				if(angleClosest2>360) {
					angleClosest2=angleClosest2-360;
				}

				rsf=rsf+90;
				if(rsf>360) {
					rsf=rsf-360;
				}
				break;
		}

		cf3=rsf;
		final2=finalAngle;
		float res1;
		float res2;
		float resf;
		float res3;
		if(angleClosest1>180) {
			res1=360-angleClosest1;
		}
		else {
			res1=angleClosest1;
		}
		if(angleClosest2>180) {
			res2=360-angleClosest2;
		}
		else {
			res2=angleClosest2;
		}
		if(res1<res2) {
			finalAngle=angleClosest1;
			resf=res1;
		}
		else {
			finalAngle=angleClosest2;
			resf=res2;
		}
		
		if(final2>180) {
			res3=360-final2;
		}
		else {
			res3=final2;
		}
		 cf1=res3;
		//finalAngle=final2;
		float r = 0;

		//r=Mathf.Rad2Deg*Mathf.Atan2(Mathf.Sin(Mathf.Deg2Rad*(res3-finalAngle)), Mathf.Cos(Mathf.Deg2Rad*(res3-finalAngle)));
		r=res3-finalAngle;
		if(r<0) {
			r=r*-1;
		}
		if(r>180) {
			r=r-180;
		}
		r=r*rs;
		if(finalAngle>final2) {
			//finalAngle=r+finalAngle;
		}
		if(final2<=180) {
			finalAngle=final2*rs;
		}
		else {
			finalAngle=180+(180-((360-final2)*rs));
		}
		//print(rs+"- "+res3+" _ "+finalAngle);
		/*
		if(finalAngle>res3) {
			r=finalAngle-res3;
		}
		else {
			r=res3-finalAngle;
		}
		if(r>180) {
			r=r-180;
		}

		if(finalAngle>res3) {
			r=finalAngle-res3;
		}
		else {
			r=res3-finalAngle;
		}
		
		if(finalAngle>180) {
			if(res3>180) {
				if(res3>finalAngle) {
					r=res3-finalAngle;
					finalAngle=finalAngle+(r*rs);
				}
				else {
					r=finalAngle-res3;
					finalAngle=finalAngle-(r*rs);
				}
			}
			else {
				if(res3>finalAngle) {
					r=res3-finalAngle;
					finalAngle=finalAngle+(r*rs);
				}
				else {
					r=finalAngle-res3;
					finalAngle=finalAngle-(r*rs);
				}
			}
		}
		else {
			if(res3>180) {

			}
			else {

			}
		}*/


		if(res3>resf) {
			//cf1=resf+((res3-resf)*rs);
			//finalAngle=cf1;
		}

		if(res3<resf) {
			//finalAngle=final2;
		}
		if(objDistLeft<60&&finalAngle>180) {
			//finalAngle=0;
		}
		if(objDistRight<60&&finalAngle<180) {
			//finalAngle=0;
		}
		if(objDistFront>60) {
			//finalAngle
		}
		/*
		if(finalAngle<360&&finalAngle>=180) {
			angleResult=angleResult+anglebias;
		}
		if(finalAngle>=0&&finalAngle<180) {
			angleResult=angleResult-anglebias;
		}
		if(finalAngle>360) {
			angleResult=360;
		}
		if(finalAngle<0) {
			angleResult=0;
		}*/
		//print(finalAngle+"!"+angleResult+" | "+closestObjIndex+" | "+objDistFront+"/"+objDistBack+"/"+objDistLeft+"/"+objDistRight);
		if(finalAngle>min1&&finalAngle<max1) {
			//print(finalAngle+"-"+angleResult+" | "+closestObjIndex);
			infoText3.text="";
			for(int i = 0;i<outputPolygon.Length;i++) {
				infoText3.text+=outputPolygon[i]+";";
			}
		}
		if(finalAngle>min2&&finalAngle<max2) {
			//print(finalAngle+"-"+angleResult+" | "+closestObjIndex);
			infoText2.text="";
			for(int i = 0;i<outputPolygon.Length;i++) {
				infoText2.text+=outputPolygon[i]+";";
			}
		}
		for(int i = 0;i<outputPolygon.Length;i++) {
			infoText.text+=outputPolygon[i]+";";
		}
	}
	#endregion

}