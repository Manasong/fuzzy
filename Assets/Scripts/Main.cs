﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour {

	PathFind pf;
	Control control;
	ObjPhysics physics;
	Text posTextX;
	Text posTextY;

	public int count2;
	public int count;
	public int max=100;

	public int ticksBetweenTrajectorySaves=100;
	public int ticks=0;
	Text pathMetrics;
	Text pathsCalculated;
	Text Trajectory;
	Text Obstacles;
	public ArrayList obs = new ArrayList();
	public bool newObs=false;
	public Node[] path;
	public bool newPath=false;
	public int pathCount = 0;
	public int nodesCount;
	public int costCount;
	public int frontierCount;

	void Start() {
		pf = GameObject.Find ("Drone").GetComponent<PathFind> ();
		control=GameObject.Find("Drone").GetComponent<Control>();
		pf.control = control;
		control.pf = pf;
		physics=GameObject.Find("Drone").GetComponent<ObjPhysics>();
		posTextX=GameObject.Find("posTextX").GetComponent<Text>();
		posTextY=GameObject.Find("posTextY").GetComponent<Text>();
		count=0;

		pathMetrics = GameObject.Find ("PathsMetrics").GetComponent<Text> ();
		pathsCalculated = GameObject.Find ("PathsCalculated").GetComponent<Text> ();
		Trajectory = GameObject.Find ("Trajectory").GetComponent<Text> ();
		Obstacles = GameObject.Find ("Obstacles").GetComponent<Text> ();
		control.main = this;
		pf.main=this;
	}

	// Update is called once per frame, goes as fast as the processor can
	void Update () {
		control.toUpdate();
	}

	//FixedUpdate is called every engine update, the frequency of the updates is always constant
	void FixedUpdate() {
		Metrics ();
		if(count<control.maxcount) {
			count++;
		}
		if(count>=control.maxcount) {
			physics.toUpdate();
			count=0;
		}

		if(count2<control.maxcount2) {
			count2++;
		}
		if(count2>=control.maxcount2) {
			posTextX.text+=control.transform.position.x+";";
			posTextY.text+=control.transform.position.z+";";
			count2=0;
		}
	}

	public void Metrics(){
		if(ticks>=ticksBetweenTrajectorySaves){
			Trajectory.text+=control.transform.position.x.ToString("n2")+","+control.transform.position.z.ToString("n2")+";";
			ticks = 0;
		}
		if(newObs){
			Obstacles.text = "";
			for(int i=0;i<obs.Count;i++){
				Obstacles.text += ((Vector2)obs [i]).y+","+((Vector2)obs [i]).x+";";
			}
			newObs=false;
		}
		if(newPath){
			string s = "";
			path=pf.path;
			for(int i=0;i<path.Length;i++){
				s += path [i].pos.x + "," + path [i].pos.y + ";";
			}
			s += ";";
			pathsCalculated.text += s;
			pathCount++;
			nodesCount=pf.nodesCount;
			costCount=pf.costCount;
			frontierCount=pf.frontierCount;
			string ss = "";
			ss+="Path Number:"+pathCount+". N# Explored Nodes:"+nodesCount+". N# Nodes on Frontier"+frontierCount+". Total Cost:"+costCount+";";
			pathMetrics.text+=ss;
			newPath=false;
		}
		ticks++;
	}
}
