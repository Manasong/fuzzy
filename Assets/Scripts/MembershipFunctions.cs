﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MembershipFunctions : MonoBehaviour{

	public float[,] MaxVelPerDist;
	public float[,] MF;
	public int Mfsize;
	public float[] frontMF;
	public float[] backMF;
	public float[] leftMF;
	public float[] rightMF;
	public float dodgeContinueThreshold;

	public void StartDefault() {
		MaxVelPerDist=new float[3, 2]; //[Points, 0=maxvel,1=distance]
		MaxVelPerDist[0, 0]=10;
		MaxVelPerDist[0, 1]=5;
		MaxVelPerDist[1, 0]=20;
		MaxVelPerDist[1, 1]=15;
		MaxVelPerDist[2, 0]=100;
		MaxVelPerDist[2, 1]=100;
		dodgeContinueThreshold=15;
		frontMF = new float[3];
		backMF = new float[3];
		leftMF = new float[3];
		rightMF = new float[3];
		frontMF [0] = 135;frontMF [1] = 180;frontMF [2] = 225;
		backMF [0] = 315;backMF [1] = 360;backMF [2] = 45;
		leftMF [0] = 45;leftMF [1] = 90;leftMF [2] = 135;
		rightMF [0] = 225;rightMF [1] = 270;rightMF [2] = 315;
		MF=new float[6, 4];
		Mfsize=6;
		MF[0, 0]=0;
		MF[0, 1]=0;
		MF[0, 2]=180;
		MF[0, 3]=0;

		MF[1, 0]=0;
		MF[1, 1]=0;
		MF[1, 2]=90;
		MF[1, 3]=3;

		MF[2, 0]=-90;
		MF[2, 1]=90;
		MF[2, 2]=270;
		MF[2, 3]=1;

		MF[3, 0]=0;
		MF[3, 1]=180;
		MF[3, 2]=360;
		MF[3, 3]=2;

		MF[4, 0]=90;
		MF[4, 1]=270;
		MF[4, 2]=450;
		MF[4, 3]=3;

		MF[5, 0]=180;
		MF[5, 1]=360;
		MF[5, 2]=0;
		MF[5, 3]=0;
	}
}
