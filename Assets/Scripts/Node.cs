﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {

	public Vector2 pos;
	public Node fatherN;
	public int cost;

	public void Start(){
		pos = new Vector2 (0, 0);
		fatherN = null;
	}
}
