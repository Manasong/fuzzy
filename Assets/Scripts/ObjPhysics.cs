﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjPhysics : MonoBehaviour {

	private Vector3 _movementVector;
	public Vector3 movementVector {
		get { return _movementVector; }
		set { _movementVector=value; }
	}
	private float _movementAngle;
	public float movementAngle {
		get { return _movementAngle; }
		set { _movementAngle=value; }
	}

	public void toUpdate() {
		Translate();
	}
	
	public void Translate() {
		transform.Translate(movementVector);
	}
}
