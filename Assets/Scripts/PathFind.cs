﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class PathFind : MonoBehaviour {

	public Control control;
	private ArrayList _obstacleMap;
	public ArrayList obstacleMap {
		get { return _obstacleMap; }
		set { _obstacleMap = value; }
	}

	private Node[] _path;
	public Node[] path {
		get { return _path; }
		set { _path = value; }
	}

	ArrayList frontier;
	ArrayList exploredNodes;
	Node currentNode;
	Vector2 targetPos;
	public int MaxCount=1000;
	Vector2 startPos;
	private bool _solutionReady;
	public bool solutionReady{
		get { return _solutionReady; }
		set { _solutionReady = value; }
	}
	Thread calculator;
	bool end=false;
	public bool stepByStep=false;
	public bool step=false;
	public Vector2 validPos=new Vector2();
	public Node validNode;
	public int progresscounter = 0;
	public int prograssMark=10000;
	public Main main;
	public int nodesCount;
	public int costCount;
	public int frontierCount;

	public void Initiate () {
		solutionReady = false;
		currentNode = new Node ();
		currentNode.Start ();
		currentNode.pos = control.getCurPos();
		currentNode.cost = 0;
		startPos = currentNode.pos;
		frontier = new ArrayList ();
		exploredNodes = new ArrayList ();
		exploredNodes.Add (currentNode);
		obstacleMap = control.obsMap;
		BuildFrontier ();
		path = null;
		targetPos = control.targetPosition;

		bool b = false;
		for(int i=0;i<obstacleMap.Count;i++){
			if(obstacleMap.Contains(currentNode.pos)){
				b = true;
			}
		}
		if(calculator!=null){
			calculator.Abort ();
		}
		if(b){
			FindNearestValidBlock ();
		}
		for(int i = 0;i<obstacleMap.Count;i++) {
			if((Vector2)obstacleMap[i]==targetPos) {
				Debug.Log("ERROR, TARGET IS IN A BLOCKED POSITION");
				b=true;
			}
		}
		if (!b) {
			calculator = new Thread (Calculate);
			calculator.Start ();
			Debug.Log ("STARTING THREAD");
		}
	}

	public void FindNearestValidBlock(){
		Debug.Log ("ERROR STARTING NODE IS OCCUPPIED:" + control.getCurCoord ().ToString () + " - " + control.getCurPos ().ToString ());
		control.validStartPosReached = false;
		control.WaitForNewValidStartPos ();
		ArrayList exploredOccupied = new ArrayList();
		ArrayList frontierOccupied = new ArrayList ();
		Node currentOccupied = new Node();
		currentOccupied.Start ();
		currentOccupied.pos = control.getCurPos();
		currentOccupied.cost = 0;
		bool bb = false;
		validNode=new Node();
		validNode.Start ();
		while (!bb) {
			Vector2[] pos = new Vector2[4];
			pos [0] = new Vector2 (currentOccupied.pos.x + 1, currentOccupied.pos.y);
			pos [1] = new Vector2 (currentOccupied.pos.x, currentOccupied.pos.y + 1);
			pos [2] = new Vector2 (currentOccupied.pos.x, currentOccupied.pos.y - 1);
			pos [3] = new Vector2 (currentOccupied.pos.x - 1, currentOccupied.pos.y);
			for (int i = 0; i < 4; i++) {
				bool b = false;
				bb = true;
				for (int j = 0; j < exploredOccupied.Count; j++) {
					if (pos [i] == (Vector2)exploredOccupied [j]) {
						b = true;
					}
				}
				for (int j = 0; j < frontierOccupied.Count; j++) {
					if (pos [i] == ((Node)frontierOccupied [j]).pos) {
						b = true;
					}
				}
				for(int j=0;j<obstacleMap.Count;j++){
					if(pos[i]==(Vector2)obstacleMap[j]){
						bb = false;
					}
				}
				if(bb){
					validNode.pos = pos[i];
				}
				if (!b) {
					Node n = new Node ();
					n.Start ();
					n.pos = pos [i];
					n.cost = currentOccupied.cost + 1;
					n.fatherN = currentOccupied;
					frontierOccupied.Add (n);
				}
			}
			int chosenFront=0;
			float minCost = 0;
			for(int i=0;i<frontierOccupied.Count;i++){
				Vector2 fPos = ((Node)frontierOccupied [i]).pos;
				if(minCost==0){
					chosenFront = i;
					minCost=((Node)frontierOccupied [i]).cost;
				}
				float fCost = ((Node)frontierOccupied [i]).cost;
				if(minCost>fCost){
					minCost = fCost;
					chosenFront = i;
				}
			}
			currentOccupied = (Node)frontierOccupied [chosenFront];
			frontierOccupied.Remove (currentOccupied);
			exploredOccupied.Add (currentOccupied.pos);
		}
		Debug.Log ("NEW VALID START POS:" + validNode.pos);
		control.validNodePos = validNode.pos;
		control.validStartPosReached=true;
		control.WaitForNewValidStartPos ();
	}

	public void DoStep(){
		step=true;
	}

	public void Calculate(){
		int counter = 0;
		while (!end){
			if(stepByStep){
				while(!step)
				{
					Debug.Log ("CN:" + currentNode.pos + " Cost:" + currentNode.cost);
					Thread.Sleep (1000);
				}
			}
			step = false;
			//Debug.Log ("ITER:"+counter);
			GetNextNode ();
			BuildFrontier ();
			counter++;
			if (MaxCount > 0) {
				if(counter > MaxCount) {
					end = true;
				}
			}
			if (currentNode.pos == targetPos) {
				end = true;
			}
			progresscounter++;
			if(progresscounter>=prograssMark){
				bool b = false;
				for(int i = 0;i<frontier.Count;i++) {
					if(((Node)frontier[i]).pos==targetPos) {
						b=true;
					}
				}
				Debug.Log (prograssMark + " ITERATIONS. COST:"+currentNode.cost+". STARPOS:"+startPos.ToString()+". POS:"+currentNode.pos+". EXPLNOD:"+exploredNodes.Count+". FRONT:"+frontier.Count+". END?:"+b);
				progresscounter = 0;
			}
		}
		end = false;
		if(counter ==MaxCount){
			Debug.Log ("ERROR MAX SEARCH ITERATION REACHED");
		}
		if(currentNode.pos==targetPos){
			Debug.Log ("SOLUTION REACHED. COST:" + currentNode.cost + "  POS:" + currentNode.pos);
		}
		Node n = currentNode;
		int numberNodes = 0;
		while (n.pos!=startPos){
			numberNodes++;
			n = n.fatherN;
		}
		n = currentNode;
		path = new Node[numberNodes];
		while (n.pos!=startPos){
			path [n.cost - 1] = n;
			n = n.fatherN;
		}
		nodesCount=exploredNodes.Count;
		costCount=currentNode.cost;
		frontierCount=frontier.Count;
		main.newPath=true;
		solutionReady = true;
	}

	public void GetNextNode(){
		int chosenFront=0;
		float minCost = 0;
		for(int i=0;i<frontier.Count;i++){
			Vector2 fPos = ((Node)frontier [i]).pos;
			float heurCost = Mathf.Sqrt (Mathf.Pow ((fPos.x - targetPos.x), 2) + Mathf.Pow ((fPos.y - targetPos.y), 2));
			if(minCost==0){
				chosenFront = i;
				minCost=heurCost+((Node)frontier [i]).cost;
			}
			float fCost = heurCost + ((Node)frontier [i]).cost;
			if(minCost>fCost){
				minCost = fCost;
				chosenFront = i;
			}
		}
		currentNode = (Node)frontier [chosenFront];
		frontier.Remove (currentNode);
		exploredNodes.Add (currentNode);
	}

	public void BuildFrontier(){
		Vector2[] pos = new Vector2[4];
		pos [0] = new Vector2 (currentNode.pos.x + 1, currentNode.pos.y);
		pos [1] = new Vector2 (currentNode.pos.x, currentNode.pos.y + 1);
		pos [2] = new Vector2 (currentNode.pos.x , currentNode.pos.y - 1);
		pos [3] = new Vector2 (currentNode.pos.x - 1, currentNode.pos.y);
		for(int i=0;i<4;i++){
			bool b = false;
			for(int j=0;j<obstacleMap.Count;j++){
				if(pos[i]==(Vector2)obstacleMap[j]){
					b = true;
				}
			}
			for (int j = 0; j < frontier.Count; j++) {
				if (pos [i] == ((Node)frontier [j]).pos) {
					b = true;
				}
			}
			for (int j = 0; j < exploredNodes.Count; j++) {
				if (pos [i] == ((Node)exploredNodes [j]).pos) {
					b = true;
				}
			}
			if(!b){
				Node n = new Node ();
				n.Start ();
				n.pos=pos[i];
				n.cost = currentNode.cost + 1;
				n.fatherN = currentNode;
				frontier.Add (n);
			}
		}
	}

	public Node[] GetPath(){
		return path;
	}

}
